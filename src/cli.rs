use clap::{Parser, ValueEnum};

#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
pub struct Cli {
  ///
  /// disabled : Disable the 'fn' key. Pressing 'fn'+'F8' will behave like you only press 'F8'
  ///
  /// normal : Function keys are used as first key. Pressing 'F8' key will behave like a F8. Pressing 'fn'+'F8' will act as special key (play/pause).
  ///
  /// special : Function keys are used as last key. Pressing 'F8' key will act as a special key. Pressing 'fn'+'F8' will behave like a F8.
  #[arg(value_enum)]
  pub mode: FunctionMode,

  #[arg(short, long)]
  #[arg(default_value_t = String::from("/sys/module/hid_apple/parameters/fnmode"))]
  pub file: String,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, ValueEnum)]
pub enum FunctionMode {
  Disabled,
  Normal,
  Special,
}