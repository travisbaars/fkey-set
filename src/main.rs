mod cli;

use std::fs::{read_to_string, write};
use std::error::Error;
use clap::Parser;

use cli::{Cli, FunctionMode};

fn main() -> Result<(), Box<dyn Error>> {

  // CLI value declarations
  let cli = Cli::parse();

  let file = cli.file;

  let mode = match cli.mode {
    FunctionMode::Disabled => "0",
    FunctionMode::Normal => "2",
    FunctionMode::Special => "1",
  };

  let file_content = read_to_string(&file)?;

  if mode == file_content.trim() {
    println!("No changes necessary.");
  } else {
    write(file, mode)?;
    println!("\n FnKey mode was changed from {} to {}", file_content.trim(), mode);
  }

  Ok(())
}