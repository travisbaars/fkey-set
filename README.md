# fKey-Set

FnKey-Set is a simple command-line utility written in Rust to effortlessly modify the `hid_apple` driver `fnmode` setting file. This tool provides a user-friendly interface for setting the desired function key behavior.

This program is really only useful if you are using an Apple Magic Keyboard with linux and are annoyed by the function keys not working like you want.

## Usage

This program is designed to be run as a standalone CLI program or as a `systemd` service that runs once at boot to set the preferred function key behavior.

A `systemd service` provides a simple way to run the program on boot. This is only necessary if you want the function key behavior to persist after shutdown or reboot.

Here is the `--help` text as a guide:

```
Usage: fkey-set <FNKEY_MODE>

Arguments:
  <FNKEY_MODE>
          disabled : Disable the 'fn' key. Pressing 'fn'+'F8' will behave like you only press 'F8'

          normal : Function keys are used as first key. Pressing 'F8' key will behave like a F8. Pressing 'fn'+'F8' will act as special key (play/pause).

          special : Function keys are used as last key. Pressing 'F8' key will act as a special key. Pressing 'fn'+'F8' will behave like a F8.

          [possible values: disabled, normal, special]

Options:
  -h, --help
          Print help (see a summary with '-h')

  -V, --version
          Print version
```

Root privileges are required for these changes to occur, A.K.A `sudo`.

## Step 1:

Download the prebuilt binary from the [Releases](https://gitlab.com/travisbaars/fkey-set/-/releases/) page.

##### OR

Clone the repository and build the program yourself:

```bash
git clone https://gitlab.com/travisbaars/fkey-set.git
```

### If building from scratch:

Move into the downloaded repository:

```bash
cd fkey-set
```

Run the cargo build command[^1][^3] with the `--release` flag:

```bash
cargo build --release
```

Run the `permissions.sh` script located in the `./scripts` directory to set the proper permissions on the built binary:

```bash
sudo ./scripts/permissions.sh
```

## Step 2:

Move[^1] the downloaded or built binary into the `/usr/bin/` directory.

```bash
sudo cp fkey-set /usr/bin/
```

##### OR

If using self-built binary:

```bash
sudo cp ./target/release/fkey-set /usr/bin/
```

## Step 3:

Download the [fkey-set.service](./service/fkey-set.service) file from the repository.

Move it to the desired location[^1]:

```bash
sudo mv fkey-set.service /etc/systemd/system/
```

##### OR

Simply copy the following into your desired location[^2]:

```systemd
[Unit]
Description=Change Apple Keyboard default Fn key behavior
After=local-fs.target
After=network.target

[Service]
Type=oneshot
RemainAfterExit=true
ExecStart=/usr/bin/fkey-set normal

[Install]
WantedBy=multi-user.target

```

If you would like a different default function key mode you can simply change the `ExecStart=` line to contain your preferred mode. You can reference the `--help` flag to see available options.

## Step 4

Enable the systemd service:

```bash
sudo systemctl enable fkey-set.service
```

If you would like to start the service so you don't have to wait until reboot run:

```bash
sudo systemctl start fkey-set.service
```

Verify the service is running correctly:

```bash
systemctl status fkey-set.service
```

[^1]: All commands are based on using Fedora as your operating system.
[^2]: The service must be placed in a location recognized by the operating system. File must be run as root se the preferred option is to store it in the `/etc/systemd/system` directory (on Fedora) for simplicity.
[^3]: Make sure you have Rust installed on your system.
