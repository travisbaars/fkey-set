#!/bin/bash

setcap cap_setuid=e target/release/fkey-set
chown root:root target/release/fkey-set
chmod o+x target/release/fkey-set